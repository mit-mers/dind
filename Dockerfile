ARG base_tag=stable-dind
FROM docker:$base_tag

COPY mers-dockerd-entrypoint.sh /usr/local/bin/

ENTRYPOINT ["mers-dockerd-entrypoint.sh"]
