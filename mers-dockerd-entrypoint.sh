#!/bin/sh

if [ -n "$DOCKER_REGISTRY_MIRRORS" ]; then
  IFS=","
  for mirror in $DOCKER_REGISTRY_MIRRORS; do
    set -- "$@" "--registry-mirror=$mirror"
  done
  unset IFS
fi

exec "dockerd-entrypoint.sh" "$@"

# Local Variables:
# sh-basic-offset: 2
# End:
